#define MAXLINE 100

typedef struct Medicion {
	char date[MAXLINE];
	char time[MAXLINE];
	float temperature;
	float humidity;
	int pressure;
	float altitude;
	float battery;
} Medicion_t;

double toCent(double far);
