#include <stdio.h>
#include <stdlib.h>
#include "medicion.h"

int main()
{
	FILE *fp;
	FILE * nuevo;
	char filename[MAXLINE];
	Medicion_t m;

	printf("Ingrese nombre archivo: ");
	scanf("%s",filename);

	fp = fopen(filename,"r");
	nuevo = fopen("medicionesCelsius.csv", "w");
	//Salta la primera linea (cabecera del archivo)
	fscanf(fp,"%*[^\n]\n");
	
	while(! feof(fp)){
		//Lee la segunda linea
		int n = fscanf(fp, "%s\t%s\t%f\t%f\t%d\t%f\t%f", m.date, m.time, &m.temperature, &m.humidity, &m.pressure, &m.altitude, &m.battery);
		
		fprintf(nuevo,"%s\t%s\t%.2f\t%f\t%d\t%f\t%f\n", m.date, m.time, toCent(m.temperature), m.humidity, m.pressure, m.altitude, m.battery);

		//Si retorna 7 o mas variables entonces se ha leido correctamente la linea
		if(n < 7)
			fprintf(stderr,"Error al leer el archivo\n");
	}
	fclose(fp);
	fclose(nuevo);
}

double toCent(double far)
{	
	double cels = (far-32.0)*(5.0/9.0);
	return cels;
}

